import React from "react";
import ReactDOM from "react-dom";

class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { time: new Date().toLocaleTimeString() };
  }

  tick() {
    this.setState(state => ({
      time: new Date().toLocaleTimeString()
    }));
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return <p> Time: {this.state.time}</p>;
  }
}

class HelloMessage extends React.Component {
  render() {
    return (
      <div>
        Hello {this.props.name} <Timer />
      </div>
    );
  }
}

ReactDOM.render(<HelloMessage name='ashu' />, document.getElementById("root"));
